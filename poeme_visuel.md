![](https://upload.wikimedia.org/wikipedia/commons/c/c6/Irene_Vilar_gary_isaacs.jpg)

Ma grand-mère


membre de la famille (P1038) - correspondant à - grand-mère (Q9235758)


Irene Vilar


Puerto Rican writer


![](https://upload.wikimedia.org/wikipedia/commons/3/3c/People_playing_board_game_card_game_in_circle.jpg)

qui jouait aux cartes


utilise  (P2283) - correspondant à - carte à jouer (Q47883)


card game


game using playing cards as the primary device


![](https://upload.wikimedia.org/wikipedia/commons/b/bc/%D0%9E%D0%BD%D0%B5%D0%B3%D0%B8%D0%BD_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0.jpg)

pendant des heures


durée (P2047) - correspondant à - 3 heure (Q25235)


Eugene Onegin


opera by Pyotr Ilyich Tchaikovsky


![](https://upload.wikimedia.org/wikipedia/commons/7/7f/Madeleine_Carroll_1938.jpg)

qui s'appelait Madeleine


prénom (P735) - correspondant à - Madeleine (Q4273929)


Madeleine Carroll


English actress (1906–1987)


![](https://upload.wikimedia.org/wikipedia/commons/e/ef/Els_Callens.jpg)

qui vivait à Anvers


résidence (P551) - correspondant à - Anvers (Q12892)


Els Callens


Belgian tennis player


![](https://upload.wikimedia.org/wikipedia/commons/5/5b/Yasmine_hilde_rens-1561466171.jpg)

morte en 2009


date de mort (P570) - correspondant à - 2009 (Q1996)


Yasmine


Belgian singer


![](https://upload.wikimedia.org/wikipedia/commons/5/56/VanOstaijen3.jpg)

enterrée à Schoonselhof


lieu de sépulture (P119) - correspondant à - cimetière de Schoonselhof (Q2248900)


Paul van Ostaijen


Belgian poet, writer (1896-1928)


![](https://upload.wikimedia.org/wikipedia/commons/f/f1/Malus_sargentii.jpg)

près d'un pommier


nom scientifique du taxon (P225) - correspondant à - Malus (Q104819)


Malus


genus of plants


