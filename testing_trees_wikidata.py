from query_materials import get_info, query
from time import sleep

q1 = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?item ?itemLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
  {
    SELECT DISTINCT ?item WHERE {
      ?item p:P31 ?statement0.
      ?statement0 (ps:P31/(wdt:P279*)) wd:Q473972.
      ?item p:P17 ?statement1.
      ?statement1 (ps:P17/(wdt:P279*)) wd:Q31.
    }
    LIMIT 100
  }
}
"""
for result in query(q1):
    info = get_info(result['item']['value'])
    try: 
        label = info['labels']['en']['value']
        if label: 
            print(info['labels']['en']['value'] + ' IS A NATURE RESERVE IN BELGIUM.')
    except KeyError:
        print ("no english label?")
    sleep(1)


# python3 script.py > output.txt (saves output in txtfile)
# python3 script.py >> output.txt (adds lines to existing files)

# poem Gijs: maak verschillende queries en combineer random choices van hen in print statement
# make instance for free form poetry, see Wikispore for making new wikiforms queer.wikispore.org
# student has to know purpose, cfr with code - save results from the query
# more smaller steps in querying and how to rewrite to get results: geef meer voorbeelden met wat niet werkt en hoe te doen werken
# rewrite into data, filtering out personal information, can be more discussed!
# interesting to get information and export it to other support outside