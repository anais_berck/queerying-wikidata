# Paged.js in a jinja template

This script shows a recipe for making a paged.js publication with Python. To avoid having to write all the HTML in the python script it uses the jinja templating engine.

## Installation

First, clone, or download a copy of this repository. 

### Python 3

The script uses Python 3. If you know you have it installed this step can be skipped. If you are not sure whether it is installed on your computer, run the following command in the terminal:

```bash
python3 --version
```

It should output the version number of the copy of Python you have installed, something like:

```
3.10.7
```

If the terminal instead prints something like:

```
Command 'python3' not found...
```

It is most likely Python isn't installed. You can [download it, and find installation instructions here](https://www.python.org/downloads/).

### Jinja

```python
pip3 install jinja2
```

## Running the script

Running the script should result in the folder of the script: `index.html` this file is based on the template `templates/template.html`.

*When using the terminal*: The script can be ran by first using `cd` to move into the folder of the script. The easiest way is typing `cd` and a space and then drag and drop the folder onto the terminal. Then press enter. Once you are in the folder, type:

```bash
python3 render-template.py
```

## Seeing the result

The generated HTML file can simply be opened in a web browser.
Pages using paged.js *and* an external styles file need to be opened through a local server.
It is quite possible your editor has this functionality built in and it is perfectly fine to use that.
Python also has a simple server built in. To start it, first `cd` into the folder of your project. Then run:

```bash
python3 -m http.server
```

The output should be something like:

```
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

Indicating a server has been started and is listening for requests on port 8000.
Open a webbrowser and open the address <http://0.0.0.0:8000/> to see the generated document.

## Next steps

### Different template 

It is possible to use a different template by adjusting the `env.get_template` replace for example the line:

```python
template = env.get_template("template.html") 
```

with:

```python
template = env.get_template("template-pagedjs.html") 
```

To load an example template that has paged.js built in.


### Extending the template

#### More data

Extensive documentation on the jinja templating engine can be found here: <https://jinja.palletsprojects.com/en/3.1.x/templates/>

The first way to extend the template is by providing extra variables, this can be done by providing extra keyword arguments in the template render call. With the python line below a placeholder `extra_placeholder` is added and becomes available in the template:

```python
rendered_template = template.render(template_placeholder="world", extra_placeholder="More information in the template")
```

In the template it is possible to refer to this placeholder with the `{{ }}` syntax, so:

```jinja
{{ extra_placeholder }}
```

#### Loops

Just like python jinja supports [loops](https://jinja.palletsprojects.com/en/3.1.x/templates/#for), repeated pieces of code, or in this case HTML.

It is important to understand that jinja places data in a template. It is not intended to generate data. To work with loops jinja needs a list (or actually an iterable), data it can loop over. In jinja the start of a loop is indicated with `{% for %}` the end of the loop with `{% endfor %}`

Imagine this render call in `render-template.py`:

```python
rendered_template = template.render(template_placeholder="world", list_of_technologies=["python", "jinja", "html", "css", "javascript", "paged.js"])
```

Now, add the following lines to the template to make jinja loop through the list and show the indvidual items in an HTML-list:

```jinja
<h2>Used technologies</h2>
<ul>
{% for technology in list_of_technologies %}
  <li>{{ technology }}</li>
{% endfor %}
</ul>
```
