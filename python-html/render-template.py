from jinja2 import Environment, FileSystemLoader, select_autoescape

# Setup a template loader, tell it in which folder it can find
# the templates
template_loader = FileSystemLoader("templates")
 
env = Environment(loader= template_loader, autoescape=select_autoescape())

# read a template file from the templates folder
# and parse it into a Template object
template = env.get_template("template.html") 

# Transform or flatten the Template object into a string (piece of text)
# The value of the placeholders is assigned through the keyword arguments
# rendered_template = template.render(template_placeholder="world")

rendered_template = template.render(template_placeholder="world", list_of_technologies=["python", "jinja", "html", "css", "javascript", "paged.js"])

# Open a file handle and write the rendered template to a file.
# A file which can be read by other programs, like a browser for example.
with open('index.html', 'w') as h:
  h.write(rendered_template)
