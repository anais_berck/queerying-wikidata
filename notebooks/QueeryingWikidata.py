#!/usr/bin/env python
# coding: utf-8

# # Que(e)rying WikiData
# 
# * https://www.anaisberck.be/wikidata-writing-exercise/
# * https://pad.constantvzw.org/p/wikidata_xpub
# * [git@constant](https://gitlab.constantvzw.org/anais_berck/queerying-wikidata), [git@xpub](https://git.xpub.nl/XPUB/queerying_wikidata)
# 
# 
# Example of the wikipedia haiku bot...
# 
# https://botsin.space/@wikipediahaiku
# 
# which includes (as of this moment -- Oct 27 10:59)
# 
# > He is married to  
# > Amy McLawhorn, and has  
# > a son named Michael
# 
# But tragically (I think), the bot doesn't link to its sources (which would make it so much more interesting as an interface INTO wikipedia / permit reading the isolated phrases in context.
# 
# Consider the case of House of Dust, an early computer assisted art project by Alison Knowles, a New York based artist associated with the Fluxus movement.
# 
# https://hub.xpub.nl/bootleglibrary/search?query=house+of+dust
# 
# ![](ahouseofdustcode_knowles.png)
# 
# * [Presentation SPARQL (french)](https://upload.wikimedia.org/wikipedia/commons/f/f7/SparQL_-_Wikidata_-_%C3%89cole_de_Biblioth%C3%A9caires_Documentalistes_-_2016-04-04.pdf)
# 

# In[10]:


from random import choice

material = ['SAND', 'DUST', 'LEAVES', 'PAPER', 'TIN', 'ROOTS', 'BRICK', 'STONE', 'DISCARDED CLOTHING', 'GLASS', 'STEEL', 'PLASTIC', 'MUD', 'BROKEN DISHES', 'WOOD', 'STRAW', 'WEEDS']

location = ['IN A GREEN, MOSSY TERRAIN', 'IN AN OVERPOPULATED AREA', 'BY THE SEA', 'BY AN ABANDONED LAKE', 'IN A DESERTED FACTORY', 'IN DENSE WOODS', 'IN JAPAN', 'AMONG SMALL HILLS', 'IN SOUTHERN FRANCE', 'AMONG HIGH MOUNTAINS', 'ON AN ISLAND', 'IN A COLD, WINDY CLIMATE', 'IN A PLACE WITH BOTH HEAVY RAIN AND BRIGHT SUN', 'IN A DESERTED AIRPORT', 'IN A HOT CLIMATE', 'INSIDE A MOUNTAIN', 'ON THE SEA', 'IN MICHIGAN', 'IN HEAVY JUNGLE UNDERGROWTH', 'BY A RIVER', 'AMONG OTHER HOUSES', 'IN A DESERTED CHURCH', 'IN A METROPOLIS', 'UNDERWATER']

light_source = ['CANDLES', 'ALL AVAILABLE LIGHTING', 'ELECTRICITY', 'NATURAL LIGHT']

inhabitants = ['PEOPLE WHO SLEEP VERY LITTLE', 'VEGETARIANS', 'HORSES AND BIRDS', 'PEOPLE SPEAKING MANY LANGUAGES WEARING LITTLE OR NO CLOTHING', 'ALL RACES OF MEN REPRESENTED WEARING PREDOMINANTLY RED CLOTHING', 'CHILDREN AND OLD PEOPLE', 'VARIOUS BIRDS AND FISH', 'LOVERS', 'PEOPLE WHO ENJOY EATING TOGETHER', 'PEOPLE WHO EAT A GREAT DEAL', 'COLLECTORS OF ALL TYPES', 'FRIENDS AND ENEMIES', 'PEOPLE WHO SLEEP ALMOST ALL THE TIME', 'VERY TALL PEOPLE', 'AMERICAN INDIANS', 'LITTLE BOYS', 'PEOPLE FROM MANY WALKS OF LIFE', 'NEGROS WEARING ALL COLORS', 'FRIENDS', 'FRENCH AND GERMAN SPEAKING PEOPLE', 'FISHERMEN AND FAMILIES', 'PEOPLE WHO LOVE TO READ']

print('')
print('A HOUSE OF ' + choice(material))
print('      ' + choice(location))
print('            USING ' + choice(light_source))
print('                  INHABITED BY ' + choice(inhabitants))
print('')


# ## Querying wikidata for materials...
# Go to [querybuilder](https://query.wikidata.org/querybuilder/) and search in the property box for Material... regardless of value, with references... And ... super slow... in fact it times out...
# 
# Show the query in Query Service reveals the text version of the query in the SPARQL language...
# 
# ```sparql
# SELECT DISTINCT ?item ?itemLabel WHERE {
#   SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
#   {
#     SELECT DISTINCT ?item WHERE {
#       {
#         ?item p:P186 ?statement0.
#         ?statement0 (ps:P186/(wdt:P279*)) _:anyValueP186.
#         FILTER(EXISTS { ?statement0 prov:wasDerivedFrom ?reference. })
#       }
#     }
#     LIMIT 100
#   }
# }
# ```
# 
# Indeed, as indicated clicking the checkbox to show IDs instead of labels makes the query actually return results, but the results then need to be clicked on to understand them...
# 
# ```sparql
# SELECT DISTINCT ?item WHERE {
#   {
#     ?item p:P186 ?statement0.
#     ?statement0 (ps:P186/(wdt:P279*)) _:anyValueP186.
#     FILTER(EXISTS { ?statement0 prov:wasDerivedFrom ?reference. })
#   }
# }
# LIMIT 100
# ```
# 
# * https://www.wikidata.org/wiki/Property:P186
# * https://www.wikidata.org/wiki/Q287
# 
# ok so I did this query...
# ```sparql
# SELECT DISTINCT ?statement0 ?p ?o WHERE {
#   {
#     ?item p:P186 ?statement0.
#     ?statement0 ?p ?o.
#   }
# }
# LIMIT 100
# ```
# 
# And I see in the result that the statement is then the thing that contains the P186 (ps:P186?) to the actual material, for instance:
# 
# |s|p|o|
# |---|---|---|
# |[wds:Q181008-966f70cd-49ff-d4eb-5036-c45c1dabbbda ps:P186 wd:Q287](http://www.wikidata.org/entity/statement/Q181008-966f70cd-49ff-d4eb-5036-c45c1dabbbda)</td><td>[wds:Q181008-966f70cd-49ff-d4eb-5036-c45c1dabbbda	ps:P186	wd:Q287](http://www.wikidata.org/prop/statement/P186)</td><td>[wds:Q181008-966f70cd-49ff-d4eb-5036-c45c1dabbbda	ps:P186	wd:Q287](http://www.wikidata.org/entity/Q287)</td></tr></table>

# In[ ]:





# In[ ]:





# ```sparql
# SELECT DISTINCT ?material WHERE {
#   {
#     ?item p:P186 ?statement0.
#     ?statement0 ps:P186 ?material.
#   }
# }
# LIMIT 100
# ```
# 
# MAGIC LINE TO ADD
# ```SPARQL
# SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en" }
# ```

# Putting the pieces together...

# ```sparql
# SELECT DISTINCT ?item ?itemLabel ?statement0 ?material ?materialLabel WHERE {
#   {
#     ?item p:P186 ?statement0.
#     ?statement0 ps:P186 ?material.
#     SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en" }
#   }
# }
# LIMIT 10
# ```
# 
# [LINK TO THIS QUERY](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20%3Fstatement0%20%3Fmaterial%20%3FmaterialLabel%20WHERE%20%7B%0A%20%20%7B%0A%20%20%20%20%3Fitem%20p%3AP186%20%3Fstatement0.%0A%20%20%20%20%3Fstatement0%20ps%3AP186%20%3Fmaterial.%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22%20%7D%0A%20%20%7D%0A%7D%0ALIMIT%2010)
# 
# Quite beautiful in a way, the query showed ethanol, clicking on the statement takes me to the page of the object in question, a thermometer indeed described as being made of glass + ethanol:
# 
# https://www.wikidata.org/wiki/Q53866756#Q53866756$d48d012a-4cf5-6f1c-c4c0-0b314d444026
# 
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Copy_of_Gallileo_Firenze_thermometer-MHS_1882-P5200181-gradient.jpg/220px-Copy_of_Gallileo_Firenze_thermometer-MHS_1882-P5200181-gradient.jpg)

# ## Linked Data
# 
# * https://www.wikidata.org/wiki/Wikidata:Data_access#Linked_Data_Interface_(URI)
# 
# Special:EntityData 
# 
# In a browser, this reveals the readable page:
# 
# * https://www.wikidata.org/wiki/Special:EntityData/Q42
# 
# But from code...

# In[6]:


from urllib.request import urlopen

# f = urlopen("https://www.wikidata.org/wiki/Special:EntityData/Q42")
# print (f.read().decode("utf-8"))

import json

data = json.load(urlopen("https://www.wikidata.org/wiki/Special:EntityData/Q42"))


# In[7]:


print (json.dumps(data, indent=2))


# In[8]:


print(urlopen("https://www.wikidata.org/wiki/Special:EntityData/Q42.ttl").read().decode("utf-8"))


# In addition to full (the default) there are two other "flavors" for the response, dump and simple.

# In[10]:


print(urlopen("https://www.wikidata.org/wiki/Special:EntityData/Q42.ttl?flavor=simple").read().decode("utf-8"))


# ## SPARQL endpoint
# 
# ```
# SELECT DISTINCT ?item ?itemLabel WHERE {
#   SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
#   {
#     SELECT DISTINCT ?item WHERE {
#       ?item p:P50 ?statement0.
#       ?statement0 (ps:P50/(wdt:P279*)) ?author.
#       FILTER(EXISTS { ?author p:31 q:5. })
#     }
#     LIMIT 10
#   }
# }
# ```
# 
# 
# https://query.wikidata.org/sparql
# 
# RDFLib can bed used to query a [remote endpoint](https://rdflib.readthedocs.io/en/stable/intro_to_sparql.html#querying-a-remote-service)
# 

# In[12]:


import rdflib


# In[13]:


g = rdflib.Graph()


# In[ ]:


qres = g.query(
    """
    SELECT ?s
    WHERE {
      SERVICE <https://query.wikidata.org/sparql> {
        ?s a ?o .
      }
    }
    LIMIT 3
    """
)

for row in qres:
    print(row.s)


# In[2]:


import rdflib
g = rdflib.Graph()


# In[3]:


results = g.query("""
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?item ?statement0 ?material WHERE
  {
    SERVICE <https://query.wikidata.org/sparql> {
        ?item p:P186 ?statement0.
        ?statement0 ps:P186 ?material.
    }
  }
LIMIT 10
""")


# In[4]:


for r in results:
    print (r)


# ## Resources
# * https://www.wikidata.org/wiki/Wikidata:Tools/For_programmers
# * It's a pity that [CuriousFacts](https://wikidata-analytics.wmcloud.org/app/CuriousFacts) is a tool for making corrections -- ie a "bug finder" positioned as some kind of celebration of unusual knowledge.
# * https://cis-india.org/openness/blog-old/people-are-knowledge and https://vimeo.com/26469276
# * https://www.wikidata.org/wiki/Wikidata:Data_access#Linked_Data_Interface_(URI)

# In[15]:


import json
json.load(urlopen("http://www.wikidata.org/wiki/Special:EntityData/Q1901094?flavor=simple"))


# This query seems great but it times out. :(

# In[29]:


import rdflib
g = rdflib.Graph()
query = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/> 
SELECT DISTINCT ?item WHERE {
  {
  SERVICE <https://query.wikidata.org/sparql> {
    SELECT DISTINCT ?item WHERE {
      ?item p:P50 ?statement0.
      ?statement0 (ps:P50/(wdt:P279*)) ?author.
      FILTER(EXISTS { ?author p:31 wd:Q5. })
    }
    LIMIT 10
  }
  }
}
"""


# In[3]:


import rdflib
g = rdflib.Graph()
query = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/> 
SELECT DISTINCT ?item WHERE {
SERVICE <https://query.wikidata.org/sparql> {
  ?item p:P186 ?statement0.
  ?statement0 (ps:P186/(wdt:P279*)) wd:Q287.
  ?authored_thing p:P50 ?statement1.
  ?statement1 (ps:P50/(wdt:P279*)) ?item.
}
}
LIMIT 10
"""


# In[4]:


for r in g.query(query):
    print (r)


# Could you ask the same but then where author is made of wood?
# 

# In[ ]:


Could you ask the same but then where author is made of wood


# In[3]:


import rdflib

def q(qstr):
    g = rdflib.Graph()
    query = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>

""" + qstr
    for r in g.query(query):
        print (r)


# In[6]:


q("""SELECT DISTINCT ?item ?itemLabel ?author ?authorLabel WHERE {
SERVICE <https://query.wikidata.org/sparql> {
{
    SELECT DISTINCT ?item WHERE {
      ?item p:P50 ?statement0.
      ?statement0 (ps:P50/(wdt:P279*)) ?author.
    }
    LIMIT 10
  }
}}
""")


# ## Wikidata module
# 
# https://pypi.org/project/Wikidata/
# 

# In[8]:


get_ipython().system('pip3 install wikidata')


# In[10]:


from wikidata.client import Client


# In[11]:


client = Client()  # doctest: +SKIP
entity = client.get('Q20145', load=True)
entity


# In[13]:


print (entity.description)


# In[15]:


image_prop = client.get('P18')


# In[16]:


image = entity[image_prop]


# In[17]:


image


# In[12]:


<wikidata.commonsmedia.File 'File:KBS "The Producers" press conference, 11 May 2015 10.jpg'>


# In[19]:


image.image_resolution
(820, 1122)


# In[20]:


image.image_url
'https://upload.wikimedia.org/wikipedia/commons/6/60/KBS_%22The_Producers%22_press_conference%2C_11_May_2015_10.jpg'


# In[21]:


from IPython.display import Image


# In[22]:


Image(url=image.image_url) 


# In[24]:


made_from_material = client.get("P186", load=True)


# In[26]:


print (made_from_material.description)


# In[30]:


for i in made_from_material:
    print (i.id, i.description)


# In[32]:


made_from_material.data


# In[40]:


print (f"https://www.wikidata.org/wiki/{made_from_material.type.name.title()}:{made_from_material.id}")


# In[41]:


def url(e):
    return f"https://www.wikidata.org/wiki/{e.type.name.title()}:{e.id}"


# In[42]:


print (url(made_from_material))


# There's an example property...
# 
# https://www.wikidata.org/wiki/Property:P1855

# In[44]:


example_prop = client.get("P1855")


# In[46]:


for ex in made_from_material[example_prop]:
    print (ex.id, ex.description)


# In[48]:


for x in made_from_material.iterlists():
    print (x)


# In[49]:


made_from_material.listvalues()


# ## Direct SPAQL queries (without rdflib, and with alternative/direct? endpoint)
# 
# https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service
# 
# > SPARQL queries can be submitted directly to the SPARQL endpoint with GET request to  
# https://query.wikidata.org/bigdata/namespace/wdq/sparql?query={SPARQL}  
# or the endpoint's alias  
# https://query.wikidata.org/sparql?query={SPARQL} .
# 
# The result is returned as XML by default, or as JSON if either the query parameter format=json or the header Accept: application/sparql-results+json are provided. See the user manual for more detailed information. RDF data can alternatively be accessed via a Linked Data Fragments[1] interface at https://query.wikidata.org/bigdata/ldf. 
# 

# In[9]:


from urllib.request import urlopen
from urllib.parse import quote as urlquote, urlencode
import json


# In[13]:


q1 = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?item ?statement0 ?material WHERE
  {
      ?item p:P186 ?statement0.
      ?statement0 ps:P186 ?material.
  }
LIMIT 10
"""


# In[6]:


# f = urlopen("https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=SELECT%20DISTINCT%20%3Fx%0AWHERE%20{%0A%20%20%3Fx%20wdt%3AP214%20%22113230702%22%0A}")
endpoint = 


# In[8]:


urlencode({"format": "json", "query": "this is a test"})


# In[24]:


def query (q, format="json", endpoint="https://query.wikidata.org/sparql"):
    p = {}
    p['format'] = format
    p['query'] = q
    f = urlopen(endpoint+"?"+urlencode(p))
    return json.load(f)


# In[4]:


f = urlopen(f"https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query={urlquote(query)}")
results = json.load(f)


# In[15]:


print (json.dumps(query(q1), indent=2))


# In[17]:


for bb in results['results']['bindings']:
    print (bb)


# In[21]:


for bb in results['results']['bindings']:
    print (bb)


# In[22]:


print (query2(q1))


# ## Non-human authors ?
# 
# [This blog post](https://blog.ash.bzh/en/sunday-query-the-200-oldest-alive-french-actresses/) from Harmonia Amanda, part of a series of "Sunday Queries" develops the idea of querying wikidata for the 200 oldest alive French actresses (known to the wikidata of course).

# In[27]:


prefixes = """
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX bd: <http://www.bigdata.com/rdf#>
"""


# [Q5](https://www.wikidata.org/wiki/Q5): Human  
# [P31](https://www.wikidata.org/wiki/Property:P31): Instance of  
# [P50](https://www.wikidata.org/wiki/Property:P50): author

# In[31]:


q2 = """
SELECT ?person WHERE {
    ?person wdt:P31 wd:Q5 .
    ?person wdt:P50 ?work .
}
LIMIT 10
"""


# In[32]:


def query (q, format="json", endpoint="https://query.wikidata.org/sparql"):
    p = {}
    p['format'] = format
    p['query'] = q
    f = urlopen(endpoint+"?"+urlencode(p))
    return json.load(f)['results']['bindings']


# In[33]:


for r in query(prefixes + q2):
    print (r)
    


# ## Non-Human
# 
# The [query builder documentation](https://www.wikidata.org/wiki/Wikidata:Query_Builder) includes an example of a search for "[Items having an occupation but not being humans](https://www.wikidata.org/wiki/Wikidata:Query_Builder#/media/File:Wikidata_SQB_Feb_2021_-_Simple_query_2.png)".

# In[35]:


qq = """
SELECT DISTINCT ?item ?statement0 ?thing WHERE {
  {
    SELECT DISTINCT ?item WHERE {
      MINUS {
        ?item p:P31 ?statement0.
        ?statement0 (ps:P31/(wdt:P279*)) wd:Q5.
      }
      ?item ^p:P50 ?statement1.
      ?statement1 ^(ps:P50/(wdt:P279*)) ?thing.
    }
    LIMIT 100
  }
}"""


# In[36]:


for r in query(prefixes + qq):
    print (r)


# * [French Authors](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fperson%20%3Fbook%20WHERE%20%7B%0A%20%20%3Fbook%20p%3AP50%20%3Fstatement0.%0A%20%20%3Fstatement0%20%28ps%3AP50%2F%28wdt%3AP279%2a%29%29%20%3Fperson.%0A%20%20%3Fperson%20p%3AP27%20%3Fstatement1.%0A%20%20%3Fstatement1%20%28ps%3AP27%2F%28wdt%3AP279%2a%29%29%20wd%3AQ142.%0A%7D%0ALIMIT%2010)
# * [More French Authors](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fperson%20%3FpersonLabel%20%3Fbook%20%3FbookLabel%20WHERE%20%7B%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cfr%22.%20%7D%0A%20%20%3Fbook%20p%3AP50%20%3Fstatement0.%0A%20%20%3Fstatement0%20%28ps%3AP50%2F%28wdt%3AP279%2a%29%29%20%3Fperson.%0A%20%20%3Fperson%20p%3AP27%20%3Fstatement1.%0A%20%20%3Fstatement1%20%28ps%3AP27%2F%28wdt%3AP279%2a%29%29%20wd%3AQ142.%0A%7D%0ALIMIT%2050)
# 
# * [Query for cat authors](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fperson%20%3FpersonLabel%20%3Fbook%20%3FbookLabel%20WHERE%20%7B%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cfr%22.%20%7D%0A%20%20%3Fbook%20p%3AP50%20%3Fstatement0.%0A%20%20%3Fstatement0%20%28ps%3AP50%2F%28wdt%3AP279%2a%29%29%20%3Fperson.%0A%20%20%3Fperson%20p%3AP31%20%3Fstatement1.%0A%20%20%3Fstatement1%20%28ps%3AP31%2F%28wdt%3AP279%2a%29%29%20wd%3AQ146.%0A%7D%0ALIMIT%2050)
# * https://en.wikipedia.org/wiki/F._D._C._Willard
# 
# * [QUery for dog authors](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fperson%20%3FpersonLabel%20%3Fbook%20%3FbookLabel%20WHERE%20%7B%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%2Cfr%22.%20%7D%0A%20%20%3Fbook%20p%3AP50%20%3Fstatement0.%0A%20%20%3Fstatement0%20%28ps%3AP50%2F%28wdt%3AP279%2a%29%29%20%3Fperson.%0A%20%20%3Fperson%20p%3AP31%20%3Fstatement1.%0A%20%20%3Fstatement1%20%28ps%3AP31%2F%28wdt%3AP279%2a%29%29%20wd%3AQ144.%0A%7D%0ALIMIT%2050)
# 
# * [Books written by Donna Haraway (Q253407)](https://query.wikidata.org/#SELECT%20%3Fbook%20%3Ftitle%0AWHERE%20%7B%0A%20%20%3Fbook%20wdt%3AP50%20wd%3AQ253407%20.%0A%20%20%3Fbook%20rdfs%3Alabel%20%3Ftitle%20.%0A%7D%0A)
# 
# * [co-authors with Donna Haraway](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fcoauthor%20%3Fcoauthor_label%20%3Ftitle%0AWHERE%20%7B%0A%20%20%3Fbook%20wdt%3AP50%20wd%3AQ253407%20.%0A%20%20%3Fbook%20rdfs%3Alabel%20%3Ftitle%20.%0A%20%20%3Fbook%20wdt%3AP50%20%3Fcoauthor%20.%0A%20%20%3Fcoauthor%20rdfs%3Alabel%20%3Fcoauthor_label%20.%0A%20%20FILTER%20%28%3Fcoauthor%20%21%3D%20wd%3AQ253407%29%20.%0A%7D%0AORDER%20BY%20%3Fcoauthor_label%0A)

# In[40]:





# In[44]:


e = client.get('Q283')


# In[47]:





# In[93]:


import re

def get_info (wdurl):
    m = re.search(r"^https?:\/\/www\.wikidata\.org\/(wiki|entity)/(?P<q>Q\d+)$", wdurl)
    if m is not None:
        qid = m.groupdict()['q']
        print ("qid", qid)
        url = f"https://www.wikidata.org/wiki/Special:EntityData/{qid}.json?flavor=simple"
        return json.load(urlopen(url))['entities'][qid]
    else:
        print("unrecognized url")


# In[94]:


url = "http://www.wikidata.org/entity/Q283"


# In[95]:


get_info(url)


# In[86]:





# In[84]:


url


# In[ ]:




