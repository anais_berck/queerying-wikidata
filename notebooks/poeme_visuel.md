![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Hippophae_rhamnoides-01_%28xndr%29.JPG)

L'argousier


Nom scientifique du taxon (P225) correspond à Hippophae rhamnoides (Q165378)


Hippophae rhamnoides


species of plant


![](https://upload.wikimedia.org/wikipedia/commons/f/f6/Coat_of_arms_of_Hiller%C3%B8d.svg)

est un buisson


Nature de l'élément (P31) correspondant à arbuste (Q2640465)


elder


heraldic figure


![](https://upload.wikimedia.org/wikipedia/commons/c/c5/Parma_dal_Duomo%2C_settembre_2014-1_%2815481932581%29.jpg)

qui vit près de la mer


carte de localisation (P1943) correspondant à mer (Q165)


Parma


city in the northern Italian region of Emilia-Romagna


![](https://upload.wikimedia.org/wikipedia/commons/f/fe/SuperSplash-Plopsa-DePanne.jpg)

à La Panne


localisation administrative (P131) correspondant à la Panne (Q674954)


SuperSplash


Water coaster in Plopsaland, Adinkerke, Belgium


![](https://upload.wikimedia.org/wikipedia/commons/a/ab/Tarzan_All_Story.jpg)

qui est connu pour les pêcheurs


occupation (P106) correspondant à pêcheur (Q331432)


Tarzan


fictional character from Edgar Rice Burroughs's Tarzan of the Apes


![](https://upload.wikimedia.org/wikipedia/commons/b/b9/Palaemon_serratus_Croazia.jpg)

de crevettes


sous-classe de (P279) correspondant à crustacé (Q25364)


shrimp


decapod crustaceans


![](https://upload.wikimedia.org/wikipedia/commons/d/da/Leander_Vyvey.jpg)

et le kitesurf


sport (P641) correspondant à kitesurf (Q219554)


Leander Vyvey


Belgian kitesurfer


![](https://upload.wikimedia.org/wikipedia/commons/c/c5/Parma_dal_Duomo%2C_settembre_2014-1_%2815481932581%29.jpg)

la mer


carte de localisation (P1943) correspondant à mer (Q165)


Parma


city in the northern Italian region of Emilia-Romagna


![](https://upload.wikimedia.org/wikipedia/commons/a/ab/2018-02-17_3m_female_finals_%28IDHM_Rostock_2018%29_by_Sandro_Halank%E2%80%93116.jpg)

invite à nager


sport (P641) correspondant à natation (Q6388)


Jana Lisa Rother


deutsche Wasserspringerin


![](https://upload.wikimedia.org/wikipedia/commons/6/61/Schleiden-meduse-2.jpg)

ça affecte les méduses


nom scientifique du taxon (P225) correspondant à Cnidaria (Q25441)


Cnidaria


phylum of animals


![](https://upload.wikimedia.org/wikipedia/commons/3/31/Great_white_shark_south_africa.jpg)

et les requins


nom scientifique du taxon (P225) correspondant à Selachimorpha (Q7372)


shark


superorder of predatory cartilaginous fish


![](https://upload.wikimedia.org/wikipedia/commons/c/cb/Ozone_altitude_UV_graph.svg)

les méduses causent des brûlures


a pour effet (P1542) correspondant à brûlure (Q170518)


ultraviolet radiation


form of electromagnetic radiation


![](https://upload.wikimedia.org/wikipedia/commons/4/4d/Gray1220.png)

pour lesquelles l'huile


médicament ou thérapie de traitement (P2176) correspondant à huile essentielle (Q170885)


abdominal pain


stomach aches


![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Hippophae_rhamnoides-01_%28xndr%29.JPG)

d'argousier peut aider


Nom scientifique du taxon (P225) correspond à Hippophae rhamnoides (Q165378)


Hippophae rhamnoides


species of plant


