#!/usr/bin/env python
# coding: utf-8

# In[12]:


import csv
from wikidata.client import Client
from PIL import Image
from urllib.request import urlopen
import subprocess


# In[13]:


from wikidata.entity import Entity


# We have passed March 20, Spring equinox, [World Storytelling day](https://en.wikipedia.org/wiki/World_Storytelling_Day)... [Patti](https://www.standaardboekhandel.be/p/a-book-of-days-9781526650986) reminded me of <https://en.wikipedia.org/wiki/Scheherazade>
# 
# ![](https://upload.wikimedia.org/wikipedia/en/thumb/1/1c/Wsdmatslarge.png/240px-Wsdmatslarge.png)
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Scheherazade.tif/lossy-page1-220px-Scheherazade.tif.jpg)
# 
# [source](https://archive.org/details/lp_scheherezade_nikolai-rimsky-korsakov-wiener-festspie/disc1/)
# [audio](https://archive.org/download/lp_scheherezade_nikolai-rimsky-korsakov-wiener-festspie/disc1/01.01.%20Scheherazade%3A%20The%20Sea%20And%20Sinbad%27s%20Ship%20%28Largo%20E%20Maestoso%20-%20Allegro%20Non%20Troppo%29.mp3)

# https://www.wikidata.org/wiki/Q216445

# 

# 

# > Scheherazade had perused the books, annals, and legends of preceding Kings, and the stories, examples, and instances of bygone men and things; indeed it was said that she had collected a thousand books of histories relating to antique races and departed rulers. She had perused the works of the poets and knew them by heart; she had studied philosophy and the sciences, arts, and accomplishments; and she was pleasant and polite, wise and witty, well read and well bred.

# In[14]:


# connect to Wikidata
wikidata = Client()

# specify image property
image_prop = wikidata.get('P18')

# start somewhere
# item = wikidata.get('Q17827973')
item = wikidata.get('Q216445')
for prop in item:
    print (prop.id, prop.label, item[prop])


# In[17]:


from wikidata.multilingual import Locale, MultilingualText

fr = Locale('fr')

print (item.label[fr])

# print (item.description[fr])


# In[18]:


list(item)


# In[19]:


from random import choice


# In[20]:


choice(list(item))


# In[21]:


p = _


# In[22]:


p.label[fr]


# In[23]:


p


# In[24]:


pronounication_prop = wikidata.get('P443')


# In[25]:


pronounication_prop


# In[26]:


from time import sleep


# In[27]:


max_count = 5
print (f"Here {max_count} random facts about {item.label}")
count = 0
while True:
    random_property = choice(list(item))
    random_object = item[random_property]

    print (random_property.label, random_object)
    if isinstance(random_object, Entity):
        print ("  the object is an entity")
    count += 1
    if count >= 5:
        break
    sleep(1)


# In[28]:


def random_related_entity (item):
    while True:
        random_property = choice(list(item))
        random_object = item[random_property]
        if isinstance(random_object, Entity):
            return random_property, random_object


# In[29]:


p, q = random_related_entity(item)


# In[30]:


print (p.label[fr], q.label[fr])


# In[31]:


print (p.label, q.label)


# In[32]:


en = Locale('en')

def llabel(item, locale):
    try:
        return item.label[locale]
    except KeyError:
        return item.label

def random_walk(item, locale=en, steps=5):
    count = 0
    while count < steps:
        p, q = random_related_entity(item)
        if locale:
            print (f"{llabel(item, locale)} {llabel(p, locale)} {llabel(q, locale)}")
        count += 1
        item = q


# In[33]:


random_walk(item)


# In[34]:


random_walk(item, locale=fr)


# In[35]:


random_walk(item, locale=Locale('nl'))


# In[36]:


tomato = wikidata.get('Q23501')

random_walk(tomato, steps=10)


# In[37]:


for prop in tomato:
    print (prop, prop.label)


# List of properties that relate to an Entity

# In[38]:


for prop in tomato:
    object = tomato.getlist(prop)
    for obj in object:
        if isinstance(obj, Entity):
            print (prop, prop.label, obj, obj.label)


# In[49]:


p_polinated_by = wikidata.get("P1703")


# In[50]:


p_polinator_of = wikidata.get("P1704")


# In[52]:


print (f"I am {tomato.label}, I am polinated by...")
for polinator in tomato.getlist(p_polinated_by):
    print (polinator, polinator.label)
    # look up the reverse...
    other_things_polinated = polinator.getlist(p_polinator_of)
    for other in other_things_polinated:
        # if other != tomato:
        print (f"  and I also polinate {other.label}")


# In[47]:


for prop in polinator:
    objs = polinator.getlist(prop)
    for obj in objs:
        print (f"{prop} {prop.label} {obj}")


# In[ ]:


for prop in polinator:
    print (prop, prop.label, polinator[prop])


# In[ ]:





# In[ ]:


display(url=polinator[image_prop].image_url)


# In[ ]:


up = polinator[wikidata.get('P171')]


# In[ ]:


for prop in up:
    print (prop, prop.label, up[prop])


# In[ ]:


up[image_prop].image_url


# 
