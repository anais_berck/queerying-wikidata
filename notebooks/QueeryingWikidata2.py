#!/usr/bin/env python
# coding: utf-8

# Que(e)rying Wikidata Part 2
# ==================================
# 
# Algolit, Brussels, 26 Jan 2023
# 
# Occupant: https://w.wiki/6G9f
# 
# Light characteristic: https://w.wiki/6G9i
# 
# https://pagedjs.org/documentation/
# 
# https://pagedjs.org/documentation/5-web-design-for-print/#page-breaks
# 
# https://pypi.org/project/Wikidata/
# 
# 
# > Search engines (like Google and Baidu) are a good example of applications that aggregate content and algorithmically return search results according to a keywords search. They promise to answer all our questions, but do not make the underlying processes (and ideology) visible that prioritize certain answers over others. In a query-driven society, search engines have become powerful mechanisms for truth-making and for our making sense of seemingly endless quantities of data, manifested as streams, and feeds — indicative of the oversaturation of information and the rise of the attention economy. 
# 
# * [Qu(e)ery Data: Winnie Soon & Geoff Cox, Aesthetic Programming](https://aesthetic-programming.net/pages/8-queery-data.html)
# 
# 
# > In reality, information monopolies such as Google have the ability to prioritize web search results on the basis of a variety of topics, such as promoting their own business interests over those of competitors of smaller companies that are less profitable advertising clients than large multinational corporations are. In this case, the clicks of users, coupled with the commercial processes that allow paid advertising to be prioritized in search results, mean that representation of women are ranked on a search engine page in ways that underscore women's historical and contemporary lack of status in society -- a direct mapping of old media traditions into new media architecture. Problematic representations and biases in classifications are not new. Critical library and information science scholars have well documented the ways in which some groups are more vulnerable that others to misrepresentation and misclassification. (p. 24)
# 
# Noble's running example is her experience with searching for subjects related to "black girls" and being outraged by the predominantly sexual / pornographic results that she received. So the problem isn't just that pornographic and racist materials exist online, but that search engines like Google prioritize them and then normalize and present as "factual" racist stereotypes, and so reinforce them. So a link (I hope) with the Recognition Machine is the way a "model" or an algorithm (such as the emotion detection model) or a photograph (as in the colonial archives) is promoted as "reflecting reality" in fact is itself a tool that shapes reality, and can serve to promote an (inherent or explicit) racist view of the world.
# 
# Safiya Umoja Noble: Algorithms of Oppression  
# <file:///home/murtaugh/Documents/books/Safiya%20Umoja%20Noble%20-%20Algorithms%20of%20Oppression_%20How%20Search%20Engines%20Reinforce%20Racism-NYU%20Press%20(2018).pdf#page=41>
# 

# In[17]:


import csv
from IPython.display import Image, display
from wikidata.client import Client


wikidata = Client()
image_prop = wikidata.get('P18')
item = wikidata.get('Q17827973')


# In[18]:


lighthouse = wikidata.get('Q639663')


# In[19]:


print (lighthouse.label)


# In[20]:


light_char = wikidata.get('P1030')


# In[21]:


print (light_char.label)


# In[22]:


lighthouse[light_char]


# In[ ]:





# In[23]:


with open("query-light.csv") as fin:
    for d in csv.DictReader(fin):
        print (d)


# In[24]:


item.description


# In[25]:


item[image_prop]


# You can use the image_url property to get the actual image URL (full size).

# In[26]:


item[image_prop].image_url


# And display it here in notebook form...

# In[27]:


Image(url=item[image_prop].image_url)


# Scan the CSV and filter out those items that have an image (using python's in operator)

# In[28]:


with open("query-light.csv") as fin:
    for d in csv.DictReader(fin):
        qid = d['item'].split('/')[-1]
        item = wikidata.get(qid)
        if image_prop in item:
            print (item, item[image_prop])


# Print them out (limit to 10, otherwise the notebook gets a bit overloaded)

# In[29]:


ii = []
count = 0
with open("query-light.csv") as fin:
    for d in csv.DictReader(fin):
        qid = d['item'].split('/')[-1]
        item = wikidata.get(qid)
        if image_prop in item:
            print (item.label)
            display(Image(item[image_prop].image_url))
            ii.append((item, item[image_prop]))
            # print (item)
            count += 1
            if count >= 10:
                break

print ("done")


# Output a simple markdown file.

# In[30]:


ii = []
count = 0
with open("query-light.md", "w") as fout:
    with open("query-light.csv") as fin:
        for d in csv.DictReader(fin):
            qid = d['item'].split('/')[-1]
            item = wikidata.get(qid)
            if image_prop in item:
                print (item.label)
                print (item.label, file=fout)
                print ("============", file=fout)
                print (f"![]({item[image_prop].image_url})", file=fout)
                print (file=fout)
                count += 1
                if count >= 10:
                    break

print ("done")


# You can have pandoc output it's default template, to then modify it. In this case we make a modified pandoc template that adds the pagedjs shim.
# 
# ```bash
# pandoc -D html > pandoc_template.html
# ```

# Edited the pandoc_template into pandoc_pagedjs_template includes pagedjs stuff.

# Now use pandoc + the edited template to turn the markdown into index.html
# 
# NB: mystyle.css is used to tweak the layout, adding pagebreaks and making sure the images fit on a page.
# 

# In[31]:


get_ipython().system('pandoc --template=pandoc_pagedjs_template.html --css mystyle.css query-light.md -o index.html')


# And start a local server to view it ... in another tab look at:
#     
# <http://localhost:8000/>
#     
# !!!To STOP THE SERVER Select Kernel->Interupt Kernel!!!!

# In[ ]:


get_ipython().system('python3 -m http.server')


# ## Random walk (unfinished)

# In[ ]:


item


# In[ ]:


for v in item.values():
    print (v)


# In[ ]:


for x in item.iterlistvalues():
    print (x)


# In[ ]:


wikidata.get("Q1570725").label


# In[ ]:


item.keys()


# In[ ]:


item.attributes


# In[ ]:


for prop in item:
    print (prop.id, prop.label)


# In[ ]:


source_of_energy = wikidata.get('P618')


# In[ ]:


src = item[source_of_energy]


# In[ ]:


src.label


# In[ ]:


item.attributes['claims']['P618']


# In[ ]:


print (item[wikidata.get('P84')].label)


# Convert back to text the statements.

# In[ ]:


from wikidata.entity import Entity

for key in item.keys():
    # print (key)
    value = item[key]
    if isinstance(value, Entity):
        print (f"{item.label} {key.label} {value.label}")
    else:
        print (f"{item.label} {key.label} {item[key]}")


# In[ ]:




