import csv
from wikidata.client import Client
from PIL import Image
from urllib.request import urlopen
import subprocess

# connect to Wikidata
wikidata = Client()

# specify image property
image_prop = wikidata.get('P18')

# create MD file with images, label and description based on csv file

original_poem = ["Ma grand-mère", "qui jouait aux cartes", "pendant des heures", "qui s'appelait Madeleine",\
                 "qui vivait à Anvers", "morte en 2009", "enterrée à Schoonselhof", "près d'un pommier"]

wikidata_poem = ["membre de la famille (P1038) - correspondant à - grand-mère (Q9235758)",\
"utilise  (P2283) - correspondant à - carte à jouer (Q47883)", "durée (P2047) - correspondant à - 3 heure (Q25235)",\
"prénom (P735) - correspondant à - Madeleine (Q4273929)", "résidence (P551) - correspondant à - Anvers (Q12892)",\
"date de mort (P570) - correspondant à - 2009 (Q1996)", "lieu de sépulture (P119) - correspondant à - cimetière de Schoonselhof (Q2248900)",\
"nom scientifique du taxon (P225) - correspondant à - Malus (Q104819)"]

count = 0
length = len(original_poem)

# open md file to write in
with open("poeme_visuel.md", "w") as destination:
    # open csv file to read data from
    with open("poeme_donnees.csv") as source:
        # check for each row in csv file
        for d in csv.DictReader(source):
            # split on / and select Q-number, last element
            qid = d['item'].split('/')[-1]
            # find Q-number in Wikidata
            item = wikidata.get(qid)
            # check if there is an image attached to Q-number
            if image_prop in item:
                # if so, use MD syntax to link image to MD file
                print (f"![]({item[image_prop].image_url})", file=destination)
                print (file=destination)
                # write first verse to file
                verse = original_poem[count]
                print(verse, file= destination)
                 # add break in MD file
                print("\n", file = destination)
                # write wikidata syntax to file
                verse_wiki = wikidata_poem[count]
                print(verse_wiki, file= destination)
                 # add break in MD file
                print("\n", file = destination)
                # find label of Q-number
                print (item.label)
                # write label to MD file
                print (item.label, file=destination)
                # add break in MD file
                print("\n", file = destination)
                # find description of Q-number
                print (item.description)
                # write desscription to MD file
                print (item.description, file=destination)
                # add break in MD file
                print("\n", file = destination)
                #print ("============", file=destination)

                # repeat this for 10 items
                count += 1
                if count >= length:
                    break

# when finished, print done
print ("done")

# You can have pandoc output it's default template, to then modify it. In this case we make a modified pandoc template that adds the pagedjs shim.
# pandoc -D html > pandoc_template.html
# Edited the pandoc_template into pandoc_pagedjs_template includes pagedjs stuff.
# Now use pandoc + the edited template to turn the markdown into index.html
# NB: mystyle.css is used to tweak the layout, adding pagebreaks and making sure the images fit on a page.

bashCommand = 'pandoc --template=pandoc_pagedjs_template.html --css mystyle.css poeme_visuel.md -o index.html'
output = subprocess.check_output(['bash','-c', bashCommand])

# run local server: python3 -m http.server
#  in another tab look at: http://localhost:8000/