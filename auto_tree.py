from query_materials import get_info, query
from time import sleep

q1 = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?item ?itemLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
  {
    SELECT DISTINCT ?item WHERE {
      ?item p:P1343 ?statement0.
      ?statement0 (ps:P1343/(wdt:P279*)) wd:Q19219752.
      ?item p:P31 ?statement1.
      ?statement1 (ps:P31/(wdt:P279*)) wd:Q16521.
      ?item p:P4000 ?statement2.
      ?statement2 (ps:P4000/(wdt:P279*)) wd:Q5118786.
    }
    LIMIT 100
  }
}
"""
for result in query(q1):
    info = get_info(result['item']['value'])
    print('A ' + info['labels']['en']['value'] + ' IS KNOWN IN DUTCH AS ' + info['labels']['nl']['value'])
    print('A ' + info['labels']['en']['value'] + ' TREE')
    print('IN THE PARK ' + info['labels']['en']['value'])
    sleep(1)