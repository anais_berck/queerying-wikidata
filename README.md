Que(e)rying Wikidata

   
Michael Murtaugh from Xpub, Anaïs Berck and queer artivist/wikimedian Z. Blace join forces to explore automatic writing experiments que(e)rying Wikidata.

Anais Berck is a pseudonym and stands for a collaboration between humans, algorithms and trees. As a collective, they open a space in which human intelligence is explored in company of plant intelligence and artificial intelligence. In this workshop Anaïs Berck is represented by the humans Gijs de Heij and An Mertens.
Wikidata is a free and open knowledge base that can be read and edited by both humans and machines. Wikidata says it acts as central storage for the structured data of its Wikimedia sister projects (e.g. Wikipedia, Wikivoyage, Wiktionary, Wikisource). While browsing this structured data one can encounter gaps related to different world visions. The notion of more-than-human for example, is absent.

By means of a series of writing exercises with and without code, we will create Wikidata-structure like stories about more-than-humans, inspired by Alison Knowles' House of Dust. If everything goes following plan, we will generate a collective booklet at the end of the workshop.
    
----

POSTER

Q1 = Universe https://www.wikidata.org/wiki/Q1
Q2 = Earth https://www.wikidata.org/wiki/Q2
Q3 = Life https://www.wikidata.org/wiki/Q3
Q4 = Death https://www.wikidata.org/wiki/Q4
Q5 = Human https://www.wikidata.org/wiki/Q5
Q6 = doesn't exist https://www.wikidata.org/wiki/Q6
Q7 = doesn't exist https://www.wikidata.org/wiki/Q7
Q8 = Happiness https://www.wikidata.org/wiki/Q8
Q13 = triskaidekaphobia https://www.wikidata.org/wiki/Q13
Q42 = Douglas Adams https://www.wikidata.org/wiki/Q42
Q7252 = Feminism  https://www.wikidata.org/wiki/Q7252
Q10884 = Tree (perennial woody plant)  https://www.wikidata.org/wiki/Q10884
Q51415 = Queer https://www.wikidata.org/wiki/Q51415

-----


11h-13h: Presentation
- 11h-11h20: Anaïs Berck intro (An)
- 11h20-11h55: Queer(ing) Wiki(data) (Z. Blace) 
- 11:55-12h: BREAK
- 12h-13h: WRITING/READING EXERCISES (see line 53)

14h-18h: Workshop
- (1) How to integrate SPARQL into Python (Michael)
- (2) How to generate html using Python (Gijs)
- (3) Use to paged.js to make a layout/PDF (Gijs)
- (4) Assemble a collective booklet

