__NOPUBLISH__

https://pad.xpub.nl/p/queeryingwikidata

27/10

Agenda 10/November 2022


TITLE: Que(e)rying Wikidata

TEXT:
    
Michael Murtaugh from Xpub, Anaïs Berck and queer artivist/wikimedian Z. Blace join forces to explore automatic writing experiments que(e)rying Wikidata.

Anais Berck is a pseudonym and stands for a collaboration between humans, algorithms and trees. As a collective, they open a space in which human intelligence is explored in company of plant intelligence and artificial intelligence. In this workshop Anaïs Berck is represented by the humans Gijs de Heij and An Mertens.
Wikidata is a free and open knowledge base that can be read and edited by both humans and machines. Wikidata says it acts as central storage for the structured data of its Wikimedia sister projects (e.g. Wikipedia, Wikivoyage, Wiktionary, Wikisource). While browsing this structured data one can encounter gaps related to different world visions. The notion of more-than-human for example, is absent.

By means of a series of writing exercises with and without code, we will create Wikidata-structure like stories about more-than-humans, inspired by Alison Knowles' House of Dust. If everything goes following plan, we will generate a collective booklet at the end of the workshop.
    
----

POSTER

Q1 = Universe https://www.wikidata.org/wiki/Q1
Q2 = Earth https://www.wikidata.org/wiki/Q2
Q3 = Life https://www.wikidata.org/wiki/Q3
Q4 = Death https://www.wikidata.org/wiki/Q4
Q5 = Human https://www.wikidata.org/wiki/Q5
Q6 = doesn't exist https://www.wikidata.org/wiki/Q6
Q7 = doesn't exist https://www.wikidata.org/wiki/Q7
Q8 = Happiness https://www.wikidata.org/wiki/Q8
Q13 = triskaidekaphobia https://www.wikidata.org/wiki/Q13
Q42 = Douglas Adams https://www.wikidata.org/wiki/Q42
Q7252 = Feminism  https://www.wikidata.org/wiki/Q7252
Q10884 = Tree (perennial woody plant)  https://www.wikidata.org/wiki/Q10884
Q51415 = Queer https://www.wikidata.org/wiki/Q51415

-----


11h-13h: Presentation
- 11h-11h20: Anaïs Berck intro (An)
- 11h20-11h55: Queer(ing) Wiki(data) (Z. Blace) 
- 11:55-12h: BREAK
- 12h-13h: WRITING/READING EXERCISES (see line 53)

14h-18h: Workshop
- (1) How to integrate SPARQL into Python (Michael)
- (2) How to generate html using Python (Gijs)
- (3) Use to paged.js to make a layout/PDF (Gijs)
- (4) Assemble a collective booklet

-----

LINKS Z
https://www.wikidata.org/wiki/Wikidata:WikiProject_LGBT
https://gratisdata.miraheze.org/wiki/Gratisdata:Main_Page

https://www.wikidata.org/wiki/Wikidata:List_of_properties
https://www.wikidata.org/wiki/Help:Items

START WITH A WRITING EXERCISE following the structure of HOD but with a nonhumansubject

___ Step 1: "FREE" WRITING - 10 min
Mini-intro / examples (An)
Think of your favourite tree

___ Step 2 - 10 min
WRITE YOUR HOD WITH YOUR TREE...

A ?NONHUMANSUBJECT
    ?PREDICATE ?OBJECT.
    ?
A small tree
   with low boughs..
   with reddish leaves
   from canada
   next to my house
   
___ Step 3 - 10 min
WRITE YOUR HOD WITH YOUR TREE using Python


___ Step 4: Mini intro to wikidata... (Michael, just enough)...  INCLUDE LIMITS OF WIKIDATA (Ambiguity) - 20 min
Example of rewriting first text (An)
REWRITE your text with wikidata (replace your relationships with properties from wikidata & replace specific terms with wikidata terms (the Q))
Iterative writing using actual properties / values of Wikidata (first just using Wikidata website, free searching)

	* tree (Q10884) → perennial woody plant
	* tree data structure (Q223655) → abstract data type
	* family tree (Q189977) → chart representing family relationships in a conventional tree structure
	* broad-leaved tree (Q148993) → any tree that has wide leaves
	* Desley J. Tree (Q21393199) → Zoologist specialising in thrips
	* Paulownia tomentosa (Q157393) : Princess tree → species of deciduous tree
	* trunk (Q193472) : tree trunk → main wooden axis of a tree
	* decision tree (Q831366) → decision support tool that uses a tree-like model of decisions and their possible consequences, including chance event outcomes, resource costs, and utility
	* One Tree Hill (Q203385) → 2003 American television drama series
	* remarkable tree (Q811534) → tree which, because of its great age, size or condition, or historical connection, is of exceptional cultural, landscape or nature conservation value
	* Hevea brasiliensis (Q156538) : rubber tree → species of plant
	* tree (Q272735) → undirected, connected and acyclic graph
	* apple (Q89) → fruit of the apple tree
	* Murraya koenigii (Q244731) → Subtropical tree with leaves used culinarily
	* Christmas tree (Q47128) → tree, often decorated, used in the celebration of Christmas
	* phylogenetic tree (Q242125) → branching diagram of evolutionary relationships between organisms
	* tree line (Q207762) → edge of the habitat at which trees are capable of growing
	* Happy Tree Friends (Q207627) → American Grunge TV series
	* dendrochronology (Q80205) : tree-ring dating → method of dating based on the analysis of patterns of tree rings
	* Quercus (Q12004) : oak tree → tree or shrub in the genus Quercus

Step 5: Mini intro with examples? (Michael / An) - 10 min
    Into (pseudo) sparql using Query Builder

-----


    (non-human as AI on Wikipedia: https://en.wikipedia.org/wiki/Non-human)
    (more-than-human as science fiction novel on Wikipedia: https://en.wikipedia.org/wiki/More_Than_Human)
    https://en.wikipedia.org/wiki/Anthropocentrism and critique of humanocentrism
    more-than-human in feminist theory: https://catalystjournal.org/index.php/catalyst/article/download/32835/25436?inline=1
    
    

Example of the "haiku bot"
https://botsin.space/@wikipediahaiku

Query builder
https://query.wikidata.org/querybuilder/?

The Search for

https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fitem%20WHERE%20%7B%0A%20%20%7B%0A%20%20%20%20SELECT%20DISTINCT%20%3Fitem%20WHERE%20%7B%0A%20%20%20%20%20%20%3Fitem%20p%3AP50%20%3Fstatement0.%0A%20%20%20%20%20%20%3Fstatement0%20%28ps%3AP50%2F%28wdt%3AP279%2a%29%29%20%3Fauthor.%0A%20%20%20%20%20%20FILTER%28EXISTS%20%7B%20%3Fauthor%20p%3A31%20wd%3AQ5.%20%7D%29%0A%20%20%20%20%7D%0A%20%20%20%20LIMIT%2010%0A%20%20%7D%0A%7D


-----

Non-Human: 
    https://www.wikidata.org/wiki/Q13023682

Different From (property)
	https://www.wikidata.org/wiki/Property:P1889

Hope Olson, women's thesaurus, a list of missing terms (in the Dewey decimal system, for example around work. Work is assumed to be paid and outside of the home). Gillian Rose → Paradoxical space, Lorraine Code → Rhetorical space.

https://hub.xpub.nl/bootleglibrary/book/97 , page 11
BOOTLEG LIBRARY LOGIN: turtle/princess <!>

A Q3947 (house) of Q165632 (dust)
A Q630790 (title page) of a Q571 (book)
A Q11396303 (printed book) of a Q35127 (website)
Q3428776 (query (precise request for information retrieval)) of Q19829125 (absence ((local) nonexistence of something (specific)))


----

HOUSE OF DUST
from random import choice

material = ['SAND', 'DUST', 'LEAVES', 'PAPER', 'TIN', 'ROOTS', 'BRICK', 'STONE', 'DISCARDED CLOTHING', 'GLASS', 'STEEL', 'PLASTIC', 'MUD', 'BROKEN DISHES', 'WOOD', 'STRAW', 'WEEDS']

location = ['IN A GREEN, MOSSY TERRAIN', 'IN AN OVERPOPULATED AREA', 'BY THE SEA', 'BY AN ABANDONED LAKE', 'IN A DESERTED FACTORY', 'IN DENSE WOODS', 'IN JAPAN', 'AMONG SMALL HILLS', 'IN SOUTHERN FRANCE', 'AMONG HIGH MOUNTAINS', 'ON AN ISLAND', 'IN A COLD, WINDY CLIMATE', 'IN A PLACE WITH BOTH HEAVY RAIN AND BRIGHT SUN', 'IN A DESERTED AIRPORT', 'IN A HOT CLIMATE', 'INSIDE A MOUNTAIN', 'ON THE SEA', 'IN MICHIGAN', 'IN HEAVY JUNGLE UNDERGROWTH', 'BY A RIVER', 'AMONG OTHER HOUSES', 'IN A DESERTED CHURCH', 'IN A METROPOLIS', 'UNDERWATER']

light_source = ['CANDLES', 'ALL AVAILABLE LIGHTING', 'ELECTRICITY', 'NATURAL LIGHT']

inhabitants = ['PEOPLE WHO SLEEP VERY LITTLE', 'VEGETARIANS', 'HORSES AND BIRDS', 'PEOPLE SPEAKING MANY LANGUAGES WEARING LITTLE OR NO CLOTHING', 'ALL RACES OF MEN REPRESENTED WEARING PREDOMINANTLY RED CLOTHING', 'CHILDREN AND OLD PEOPLE', 'VARIOUS BIRDS AND FISH', 'LOVERS', 'PEOPLE WHO ENJOY EATING TOGETHER', 'PEOPLE WHO EAT A GREAT DEAL', 'COLLECTORS OF ALL TYPES', 'FRIENDS AND ENEMIES', 'PEOPLE WHO SLEEP ALMOST ALL THE TIME', 'VERY TALL PEOPLE', 'AMERICAN INDIANS', 'LITTLE BOYS', 'PEOPLE FROM MANY WALKS OF LIFE', 'NEGROS WEARING ALL COLORS', 'FRIENDS', 'FRENCH AND GERMAN SPEAKING PEOPLE', 'FISHERMEN AND FAMILIES', 'PEOPLE WHO LOVE TO READ']

print('')
print('A HOUSE OF ' + choice(material))
print('      ' + choice(location))
print('            USING ' + choice(light_source))
print('                  INHABITED BY ' + choice(inhabitants))
print('')from random import choice

material = ['SAND', 'DUST', 'LEAVES', 'PAPER', 'TIN', 'ROOTS', 'BRICK', 'STONE', 'DISCARDED CLOTHING', 'GLASS', 'STEEL', 'PLASTIC', 'MUD', 'BROKEN DISHES', 'WOOD', 'STRAW', 'WEEDS']

location = ['IN A GREEN, MOSSY TERRAIN', 'IN AN OVERPOPULATED AREA', 'BY THE SEA', 'BY AN ABANDONED LAKE', 'IN A DESERTED FACTORY', 'IN DENSE WOODS', 'IN JAPAN', 'AMONG SMALL HILLS', 'IN SOUTHERN FRANCE', 'AMONG HIGH MOUNTAINS', 'ON AN ISLAND', 'IN A COLD, WINDY CLIMATE', 'IN A PLACE WITH BOTH HEAVY RAIN AND BRIGHT SUN', 'IN A DESERTED AIRPORT', 'IN A HOT CLIMATE', 'INSIDE A MOUNTAIN', 'ON THE SEA', 'IN MICHIGAN', 'IN HEAVY JUNGLE UNDERGROWTH', 'BY A RIVER', 'AMONG OTHER HOUSES', 'IN A DESERTED CHURCH', 'IN A METROPOLIS', 'UNDERWATER']

light_source = ['CANDLES', 'ALL AVAILABLE LIGHTING', 'ELECTRICITY', 'NATURAL LIGHT']

inhabitants = ['PEOPLE WHO SLEEP VERY LITTLE', 'VEGETARIANS', 'HORSES AND BIRDS', 'PEOPLE SPEAKING MANY LANGUAGES WEARING LITTLE OR NO CLOTHING', 'ALL RACES OF MEN REPRESENTED WEARING PREDOMINANTLY RED CLOTHING', 'CHILDREN AND OLD PEOPLE', 'VARIOUS BIRDS AND FISH', 'LOVERS', 'PEOPLE WHO ENJOY EATING TOGETHER', 'PEOPLE WHO EAT A GREAT DEAL', 'COLLECTORS OF ALL TYPES', 'FRIENDS AND ENEMIES', 'PEOPLE WHO SLEEP ALMOST ALL THE TIME', 'VERY TALL PEOPLE', 'AMERICAN INDIANS', 'LITTLE BOYS', 'PEOPLE FROM MANY WALKS OF LIFE', 'NEGROS WEARING ALL COLORS', 'FRIENDS', 'FRENCH AND GERMAN SPEAKING PEOPLE', 'FISHERMEN AND FAMILIES', 'PEOPLE WHO LOVE TO READ']

print('')
print('A HOUSE OF ' + choice(material))
print('      ' + choice(location))
print('            USING ' + choice(light_source))
print('                  INHABITED BY ' + choice(inhabitants))
print('')

----

Sometimes a disconnect between discourse about 'diverse knowledge' versus examples given
Example from a recent wikidata presentation:
    https://wikidata-analytics.wmcloud.org/app/CuriousFacts
But it's a bug checker (so the idea is to "catch" bad information and correct it, rather than necessarily celebrate unusal
Contractions in Wikipedia/mediawiki : Good Single
? Mention other projects? Just for the Record... /  edit-a-thons

(old idea)
- Introduction to sparql/Examples of outcomes / process / "preview" - working with hands on queries with popular/no results (pasting in pads)

----------------------

18/10

Workshop needs time to prepare it well.

Organize
Clear budget....

What is the role of the different participants within the project.
What do we expect / hope to find from each other.

Half an hour of general introduction about wikidata.
Žjelko is lobbying for queer and feminist data on the platform. Would be a nice perspective, also for the students.

Anaïs Berck explored in the algoliterary publishing house a speculatice infrastructure where humans, algorithms and trees meet.

Introduction via a favorite tree....
From there, go to Wiki Data ...


What is queer data?
What does it mean queering techniques
(Cqrellations / Queerelations...)


Algolit / Anaïs Bercks experiments / works with and activates algorithms beyond contexts of productivity.
-> presentation of Wikidata project for diVersions
https://di.versions.space/when-organic-trees-meet-the-data-tree/index.html
http://paseo-por-arboles.algoliterarypublishing.net/en

Sparql departs from a hyper definedness (?).

- Introduction to sparql
- Create a query 
- Export results to paged.js

What does it mean for queer data to be part of platform?
https://en.wikipedia.org/wiki/Semantic_Web#Challenges
AI as literature
Jannet Murray
you can't have literature if you eliminiate the 'challenges'
can we use sparql in such a way that it does not erase these challenges?
in contradiction with 'single source of knowledge'?
-> Has Z examples of tension: data that doesn't want to be encoded, people resisting encoding their data???

semantic web as ultimate surveillance devise...
how to resist that?
Ellen Ullman's example of AIDS activist group resisting making a database...
Publish lies? Participating in the semantic web, but publishing multiplicity.

Not everything can be encoded without multiplicity.

Git repository with 4 starting points
-> boilerplate paged.js

DO THIS WORKSHOP....
USE AS A TEST CASE / DEBRIEF after to brainstorm about potential followup

320 per person (full day)
160 per person (half day / presentation only)
+ train (book a week in advance)

NEXT MEETING: (An, Michael, Gijs, Z? remote)
CONSTANT 27 OCT @ 14 CET
An prepares a text?
Michael wants to make a poster + screen to promote in WDKA at least 1 week before.


-----

Studying queries Wikidata
https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial

In Wikidata, most properties are “has”-kind properties
Taste is subjective, so Wikidata doesn’t have a property for it. Instead, let’s think about parent/child relationships, which are mostly unambiguous.

On Wikidata, items and properties are not identified by human-readable names like “father” (property) or “Bach” (item). (For good reason: “Johann Sebastian Bach” is also the name of a German painter, and “Bach” might also refer to the surname, the French commune, the Mercury crater, etc.) Instead, Wikidata items and properties are assigned an identifier. To find the identifier for an item, we search for the item and copy the Q-number of the result that sounds like it’s the item we’re looking for (based on the description, for example). To find the identifier for a property, we do the same, but search for “P:search term” instead of just “search term”, which limits the search to properties. This tells us that the famous composer Johann Sebastian Bach is Q1339, and the property to designate an item’s father is P:P22.

And last but not least, we need to include prefixes. For simple WDQS triples, items should be prefixed with wd:, and properties with wdt:
    
 In the query.wikidata.org query editor, you can press Ctrl+Space (or Alt+Enter or Ctrl+Alt+Enter) at any point in the query and get suggestions for code that might be appropriate; select the right suggestion with the up/down arrow keys, and press Enter to select it.
 And autocompletion can also search for you. If you type one of the Wikidata prefixes, like wd: or wdt:, and then just write text afterwards, Ctrl+Space will search for that text on Wikidata and suggest results. wd: searches for items, wdt: for properties. 
 
Instances & classes:
    To help you to figure about the difference, you can try to use two different verbs: "is a" and "is a kind of". If "is a kind of" works (e.g. A film "is a kind of" work of art), it indicates that you are talking about a subclass, a specialization of a broader class and you should use subclass of (P279). If "is a kind of" does not work (e.g. the sentence Gone with the wind "is a kind of" film does not make sense), it indicates that you are talking about a particular instance and you should use instance of (P31).

how to search for all works of art, or all buildings, or all human settlements: the magic incantation wdt:P31/wdt:P279*, along with the appropriate class.

Look for query that works, check properties of the found items, go back
ex is instance of park, country US

https://varia.zone/en/daap-varia.html
https://lozanarossenova.com/

Consider to register on Wikipedia.org 
and use the same user name and password
to paste your POETRY WORK 
in Wikispore.org as a wiki page 
https://wikispore.wmflabs.org/wiki/Poetry:OliveTree

If you wanna learn Wikidata at your own pace here is nice material 
developed for american BA level students 
https://dashboard.wikiedu.org/training/wikidata-professional
 
______ QUERY: please give me all trees that are composers:
SELECT ?tree ?tree_label
WHERE 
{
  ?tree wdt:P31 wd:Q10884;  # is instance of tree, perrenial wood plant
        wdt:P106 wd:Q36834. # has occupation, composer
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". 
}
}

______ QUERY: give me all trees made from wood
SELECT ?tree ?tree_label
WHERE 
{
  ?tree wdt:P31 wd:Q10884;  # is instance of tree, perrenial wood plant
        wdt:P186 wd:Q287.   # made from material, wood
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". 
}
  }
  
  
--------

Exercise: https://www.anaisberck.be/wikidata-writing-exercise/

____ STEP 1: DESCRIBE YOUR FAVOURITE TREE - 10 min

The tree of my childhood is a chestnut tree.
It was planted when I was 5 years old by a friend of my parents.
It must have had my age when it was planted, as it grew as a daughter of the mother tree in the friends' garden.
The branches are sturdy.
It lived in the center at the back of the garden.
Sometimes it felt that it was keeping an eye on the house and all human beings living in it.
Every year it gave a lot of fruits.
We would gather the fruits and eat them raw or baked in the oven.
The soil around the tree would then be scattered with prickling shells.
As I used to lay down next to tree, from September till Spring this became impossible.
At the age of 45, the tree was cut, in order to make place for more houses and a street.
This was in 2018.
I did not witness the cutting. We found out after it was done.
When I called the company to see if I could reuse the wood for a bookshelve, their answer was clear.
The wood was so useless, that it went straight to a woodchipper.
The wood must have been used to fuel a house or a factory.


____ STEP 2 : REWRITE YOUR DESCRIPTION AS HOUSE OF DUST - 10 min

A chestnut tree 
	in the back of the garden
	near a house
	planted by a friend
	with sturdy wooden branches
	producing a lot of fruits
	died at the age of 45 in 2018
	buried in a woodchipper machine
	serving the heating of other beings.


____ STEP 3 : ENCODE YOUR HOD IN PYTHON, ADAPTING THE EXISTING SCRIPT - 10 min

from random import choice

non_human_subject = ["CHESTNUT", "OAK", "ASH"]

material = ['SAND', 'DUST', 'LEAVES', 'PAPER', 'TIN', 'ROOTS', 'BRICK', 'STONE', 'DISCARDED CLOTHING', 'GLASS', 'STEEL', 'PLASTIC', 'MUD', 'BROKEN DISHES', 'WOOD', 'STRAW', 'WEEDS']

location = ['IN THE PROVINCE OF ANTWERP','IN THE BACK OF A GARDEN', 'NEAR A HOUSE', 'IN A GREEN, MOSSY TERRAIN', 'IN AN OVERPOPULATED AREA', 'BY THE SEA', 'BY AN ABANDONED LAKE', 'IN A DESERTED FACTORY', 'IN DENSE WOODS', 'IN JAPAN', 'AMONG SMALL HILLS', 'IN SOUTHERN FRANCE', 'AMONG HIGH MOUNTAINS', 'ON AN ISLAND', 'IN A COLD, WINDY CLIMATE', 'IN A PLACE WITH BOTH HEAVY RAIN AND BRIGHT SUN', 'IN A DESERTED AIRPORT', 'IN A HOT CLIMATE', 'INSIDE A MOUNTAIN', 'ON THE SEA', 'IN MICHIGAN', 'IN HEAVY JUNGLE UNDERGROWTH', 'BY A RIVER', 'AMONG OTHER HOUSES', 'IN A DESERTED CHURCH', 'IN A METROPOLIS', 'UNDERWATER']

actors = ['MY PARENTS', 'A FRIEND', 'A SQUIRREL', 'WWF']

characteristics = ['STURDY BRANCHES', 'LONG SERRATED LEAVES', 'MOSSY BARK', 'PRICKLING FRUITS', 'ABUNDANT CHESTNUTS', 'EYES ON THE NEARBY HOUSE']

cause_of_death = ['LUMBERJACK', 'FUNGI', 'OLD AGE', 'DROUGHT']

serving = ['PEOPLE WHO SLEEP VERY LITTLE', 'VEGETARIANS', 'HORSES AND BIRDS', 'PEOPLE SPEAKING MANY LANGUAGES WEARING LITTLE OR NO CLOTHING', 'ALL RACES OF MEN REPRESENTED WEARING PREDOMINANTLY RED CLOTHING', 'CHILDREN AND OLD PEOPLE', 'VARIOUS BIRDS AND FISH', 'LOVERS', 'PEOPLE WHO ENJOY EATING TOGETHER', 'PEOPLE WHO EAT A GREAT DEAL', 'COLLECTORS OF ALL TYPES', 'FRIENDS AND ENEMIES', 'PEOPLE WHO SLEEP ALMOST ALL THE TIME', 'VERY TALL PEOPLE', 'AMERICAN INDIANS', 'LITTLE BOYS', 'PEOPLE FROM MANY WALKS OF LIFE', 'NEGROS WEARING ALL COLORS', 'FRIENDS', 'FRENCH AND GERMAN SPEAKING PEOPLE', 'FISHERMEN AND FAMILIES', 'PEOPLE WHO LOVE TO READ']

print('')
print('A ' + choice(non_human_subject) + ' OF ' + choice(material))
print('      ' + choice(location))
print('            WITH ' + choice(characteristics))
print('                  PLANTED BY ' + choice(actors))
print('                              DIED IN 2018 AT THE AGE OF 45 BECAUSE OF ' + choice(cause_of_death))
print('                                          BENEFITING ' + choice(serving))
print('')


____ STEP 4 : REWRITE YOUR HOUSE OF DUST USING PROPERTIES AND INSTANCES OF WIKIDATA - 20 min
    
https://www.wikidata.org/wiki/Wikidata:List_of_properties) (P-values)
https://www.wikidata.org/wiki/Help:Items (Q-values)

Query for trees (Q10884)
	is an individual of taxon (P10241), chestnut (Q22699)
	made from material (P186), plastic (Q11474)
	location (P276), province of Antwerp (Q1116)
	relative to (P2210), a human (Q5)
	has part (P527), branches (Q2923673)
	date of death (P570), 2018 (Q25291)
	cause of death (P509), lumberjack (Q1124183)  
	place of burial (P119), woodchipper (Q1621575)
	used by (P1535), humans (Q5)

____ STEP 5: USE QUERY BUILDER TO WRITE SPRQLE CODE - 10 min
https://query.wikidata.org/querybuilder/?

The below query gives the following results:
    1. https://www.wikidata.org/wiki/Q55394478 - Sony camcorder
    2. https://www.wikidata.org/wiki/Q106524215 - remarkable chestnut tree in Bulgaria
	next items are mainly individual chestnut trees in different regions
It could be interesting to query the different lines separately and create a collage...

SELECT DISTINCT ?item ?itemLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE]". }
  {
    SELECT DISTINCT ?item WHERE {
      {
        ?item p:P10241 ?statement0.
        ?statement0 (ps:P10241/(wdt:P279*)) wd:Q22699.
      }
      UNION
      {
        ?item p:P186 ?statement1.
        ?statement1 (ps:P186/(wdt:P279*)) wd:Q11474.
      }
      UNION
      {
        ?item p:P276 ?statement2.
        ?statement2 (ps:P276/(wdt:P279*)) wd:Q1116.
      }
      UNION
      {
        ?item p:P2210 ?statement3.
        ?statement3 (ps:P2210/(wdt:P279*)) wd:Q5.
      }
      UNION
      {
        ?item p:P527 ?statement4.
        ?statement4 (ps:P527/(wdt:P279*)) wd:Q2923673.
      }
      UNION
      {
        ?item p:P570 ?statement_5.
        ?statement_5 psv:P570 ?statementValue_5.
        ?statementValue_5 wikibase:timePrecision ?precision_5.
        hint:Prior hint:rangeSafe "true"^^xsd:boolean.
        FILTER(?precision_5 >= 9 )
        ?statementValue_5 wikibase:timeValue ?P570_5.
        hint:Prior hint:rangeSafe "true"^^xsd:boolean.
        FILTER(("+2018-00-00T00:00:00Z"^^xsd:dateTime <= ?P570_5) && (?P570_5 < "+2019-00-00T00:00:00Z"^^xsd:dateTime))
      }
      UNION
      {
        ?item p:P119 ?statement6.
        ?statement6 (ps:P119/(wdt:P279*)) wd:Q1621575.
      }
      UNION
      {
        ?item p:P1535 ?statement7.
        ?statement7 (ps:P1535/(wdt:P279*)) wd:Q5.
      }
    }
    LIMIT 100
  }
}
