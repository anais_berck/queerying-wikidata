let lookup = document.querySelector("#lookup");
let q = document.querySelector("input#q");
let img = document.querySelector("#image");
let results_div = document.querySelector("#results");
let results_template = document.querySelector("#result");
results_template.style.display = "none";
function show_results (data) {
    let results = results_template.cloneNode(true);
    results.removeAttribute("id");
    results.classList.add("result");
    results.style.display="block";
    results_div.insertBefore(results, results_div.firstChild);
    if (data.label) {
        let item_link = document.createElement("a");
        item_link.innerHTML = data.label;
        item_link.href = data.url;
        results.querySelector("h2").innerHTML = ""
        results.querySelector("h2").appendChild(item_link);
    }
    if (data.image) {
        results.querySelector("img").src = data.image;
    }
    let prop_template = results.querySelector("details.prop");
    let prop_parent = prop_template.parentNode;
    prop_template.style.display = "none";
    data.props.forEach((prop) => {
        let prop_details = prop_template.cloneNode(true);
        prop_details.style.display = "block";
        prop_parent.appendChild(prop_details);
        let values_div = prop_details.querySelector("ul.values");
        let property_link = document.createElement("a");
        let summary = prop_details.querySelector("summary");
        summary.innerHTML = "";
        summary.appendChild(property_link);
        property_link.href = prop.url;
        property_link.innerHTML = prop.label;
        let inverse_link = document.createElement("a");
        inverse_link.innerHTML = "INV";
        inverse_link.href = "#inverse";
        // summary.appendChild(document.createTextNode(" "));
        // summary.appendChild(inverse_link);

        let inverse_details = document.createElement("details");
        let inverse_details_summary = document.createElement("summary");
        inverse_details_summary.appendChild(inverse_link);
        let inverse_details_div = document.createElement("ul");
        inverse_details.appendChild(inverse_details_summary);
        inverse_details.appendChild(inverse_details_div);
        let inverse_loaded = false;

        inverse_link.addEventListener("click", (event) => {
            event.preventDefault();
            if (inverse_loaded) {
                return;
            }
            inverse_loaded = true;
            console.log("loading inverse property values", prop.id);
            fetch(`/get/inverse?q=${data.id}&p=${prop.id}`)
              .then((resp) => resp.json())
              .then((inv_prop_data) => {
                console.log("inv_prop_data", inv_prop_data);
                inv_prop_data.results.forEach((d)=> {
                    // console.log("d", d);
                    let ili = document.createElement("li");
                    let ila = document.createElement("a");
                    ila.innerHTML = d.label;
                    ila.href = d.url;
                    ila.addEventListener("click", (event) => {
                        event.preventDefault();
                        q.value = d.id;
                    })
                    ili.appendChild(ila);
                    inverse_details_div.appendChild(ili);
                })
              });
        });
        prop.values.forEach((value) => {
            let li = document.createElement("li");
            let link = document.createElement("a");
            li.appendChild(link);
            values_div.appendChild(li);
            link.innerHTML = value.label;
            link.href = `#${value.id}`;
            link.addEventListener("click", (event) => {
                q.value = value.id;
                event.preventDefault();
            });
        });
        values_div.appendChild(inverse_details);
    })
}
let progress = document.querySelector("#progress");
lookup.addEventListener("click", (event) => {
    let url = "/get?q="+encodeURIComponent(q.value);
    // console.log("url", url);
    event.preventDefault();
    progress.classList.add("show");
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        console.log("data", data);
        progress.classList.remove("show");
        if (data.error) {
            console.log("ERROR", data.error);
            return;
        } else {
            show_results(data);
        }
      });
});


