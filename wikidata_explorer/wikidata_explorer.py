from flask import Flask
from flask import render_template
from flask import request

from wikidata.client import Client
from wikidata.entity import Entity
from wikidata.multilingual import Locale, MultilingualText
from wikidata.datavalue import DatavalueError


app = Flask(__name__)

wikidata = Client()

# specify image property
image_prop = wikidata.get('P18')
fr = Locale('fr')
nl = Locale('nl')


@app.route("/")
def hello_world():
    ctx = {'name': 'michael'}
    return render_template('wikidata_explorer.html', **ctx)

def wikidata_url_for (v):
    if v.id.startswith("P"):
        return f"https://www.wikidata.org/wiki/Property:{v.id}"
    else:
        return f"https://www.wikidata.org/wiki/{v.id}"

def wikidata_id_from_url (url):
    ret = url.split("/")[-1]
    if ret.startswith("Property:"):
        ret = ret[9:]
    return ret

def jsonify_value (v):
    if isinstance(v, Entity):
        return {'label': str(v.label), 'id': v.id, 'url': wikidata_url_for(v), 'type': 'entity'}
    else:
        return None


@app.route("/get", methods=["GET", "POST"])
def get_api():
    # user = get_current_user()
    q = request.args.get("q", "Q14328596").strip()
    wikidata = Client()
    # try:
    qobj = wikidata.get(q)
    ret = {'id': qobj.id}
    ret['url'] = wikidata_url_for(qobj);
    ret['label'] = str(qobj.label)
    if image_prop in qobj:
        ret['image'] = qobj[image_prop].image_url
    ret['props'] = props = []
    limit = None
    count = 0
    for prop in qobj:
        try:
            objs = [jsonify_value(x) for x in qobj.getlist(prop)]
            objs = [x for x in objs if x]
        except DatavalueError:
            objs = []
        if objs:
            count += 1
            props.append({
                'id': prop.id,
                'url': wikidata_url_for(prop),
                'label': str(prop.label),
                'values': objs
            })
            if limit is not None and count >= limit:
                break
    return ret
    # except Exception as e:
    #     return {'error': str(e)}

from urllib.request import urlopen
from urllib.parse import urlencode
import json

def wikidata_query (q, format="json", endpoint="https://query.wikidata.org/sparql"):
    p = {}
    p['format'] = format
    p['query'] = q
    f = urlopen(endpoint+"?"+urlencode(p))
    return json.load(f)

@app.route("/get/inverse", methods=["GET", "POST"])
def get_inverse_api():
    # user = get_current_user()
    q = request.args.get("q", "Q146555").strip()
    p = request.args.get("p", "P171").strip()

    query = f"""SELECT ?item WHERE {{ ?item wdt:{p} wd:{q}. }} LIMIT 25"""
    ids = [];
    wikidata = Client()
    for result in wikidata_query(query)['results']['bindings']:
        id = wikidata_id_from_url(result['item']['value'])
        wobj = wikidata.get(id)
        ids.append(jsonify_value(wobj));
    return {'results': ids}