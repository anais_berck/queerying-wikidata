# import rdflib
# g = rdflib.Graph()
# https://stackoverflow.com/questions/68824808/query-wikidata-rest-api-with-related-identifier

from urllib.request import urlopen
from urllib.parse import urlencode
import json
import re


def get_info (wdurl):
    m = re.search(r"^https?:\/\/www\.wikidata\.org\/(wiki|entity)/(?P<q>Q\d+)$", wdurl)
    if m is not None:
        qid = m.groupdict()['q']
        # print ("qid", qid)
        url = f"https://www.wikidata.org/wiki/Special:EntityData/{qid}.json?flavor=simple"
        return json.load(urlopen(url))['entities'][qid]
    else:
        print("unrecognized url")

def query (q, format="json", endpoint="https://query.wikidata.org/sparql"):
    p = {}
    p['format'] = format
    p['query'] = q
    f = urlopen(endpoint+"?"+urlencode(p))
    return json.load(f)['results']['bindings']


q1 = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?item ?statement0 ?material WHERE
  {
      ?item p:P186 ?statement0.
      ?statement0 ps:P186 ?material.
  }
LIMIT 10
"""

if __name__ == '__main__':
    print (q1)
    print ()
    from time import sleep

    for result in query(q1):
        statement = result['statement0']['value']
        value, item = result['material']['value'], result['item']['value']
        value = get_info(value)
        item = get_info(item)
        # print ("value", value)
        # print ("item", item)
        try:
            material_label, item_label = value['labels']['en']['value'], item['labels']['en']['value']
            print (f"A {item_label} made of {material_label}. See {statement}")
            print ()
            sleep(3)
        except KeyError:
            print ("no english label?")